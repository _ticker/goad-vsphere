packer {
  required_plugins {
    windows-update = {
      version = "0.14.1"
      source  = "github.com/rgl/windows-update"
    }
  }
}

variable "vsphere_disk_size" {
  type    = string
  default = "61440"
}

variable "vsphere_host" {
  type    = string
  default = env("GOVC_HOST")
}

variable "vsphere_username" {
  type    = string
  default = env("GOVC_USERNAME")
}

variable "vsphere_password" {
  type      = string
  default   = env("GOVC_PASSWORD")
  sensitive = true
}

variable "vsphere_esxi_host" {
  type    = string
  default = env("VSPHERE_ESXI_HOST")
}

variable "vsphere_datacenter" {
  type    = string
  default = env("GOVC_DATACENTER")
}

variable "vsphere_cluster" {
  type    = string
  default = env("GOVC_CLUSTER")
}

variable "vsphere_datastore" {
  type    = string
  default = env("GOVC_DATASTORE")
}

variable "vsphere_folder" {
  type    = string
  default = env("VSPHERE_TEMPLATE_FOLDER")
}

variable "vsphere_management_network" {
  type    = string
  default = env("VSPHERE_MANAGE_VLAN")
}

variable "vsphere_goad_network" {
  type    = string
  default = env("VSPHERE_GOAD_VLAN")
}

variable "vagrant_box" {
  type = string
}

source "vsphere-iso" "windows-2016-amd64" {
  CPUs          = 4
  RAM           = 4096
  guest_os_type = "windows9Server64Guest"
  floppy_files = [
    "provision-autounattend.ps1",
    "provision-openssh.ps1",
    "provision-psremoting.ps1",
    "provision-pwsh.ps1",
    "provision-vmtools.ps1",
    "provision-winrm.ps1",
    "tmp/windows-2016-vsphere/autounattend.xml",
  ]
  iso_paths = [
    "[${var.vsphere_datastore}] iso/en_windows_server_2016_x64_dvd_9718492.iso",
    "[${var.vsphere_datastore}] iso/VMware-tools-windows-12.1.0-20219665.iso",
  ]
  network_adapters {
    network      = var.vsphere_management_network
    network_card = "vmxnet3"
  }

  disk_controller_type = ["pvscsi"]
  storage {
    disk_size             = var.vsphere_disk_size
    disk_thin_provisioned = true
  }
  convert_to_template = false
  insecure_connection = true
  vcenter_server      = var.vsphere_host
  username            = var.vsphere_username
  password            = var.vsphere_password
  host                = var.vsphere_esxi_host
  datacenter          = var.vsphere_datacenter
  cluster             = var.vsphere_cluster
  datastore           = var.vsphere_datastore
  folder              = var.vsphere_folder
  vm_name             = "windows-2016-amd64-vsphere"
  shutdown_command    = "shutdown /s /t 0 /f /d p:4:1 /c \"Packer Shutdown\""
  communicator        = "winrm"
  winrm_password      = "vagrant"
  winrm_username      = "vagrant"
  winrm_timeout       = "4h"
  ip_wait_timeout     = "2h"
}

build {
  sources = ["source.vsphere-iso.windows-2016-amd64"]

  provisioner "powershell" {
    use_pwsh = true
    script   = "disable-windows-updates.ps1"
  }

  provisioner "powershell" {
    use_pwsh = true
    script   = "provision.ps1"
  }

  provisioner "powershell" {
    use_pwsh = true
    script   = "enable-remote-desktop.ps1"
  }

  provisioner "powershell" {
    use_pwsh = true
    script   = "provision-cloudbase-init.ps1"
  }

  provisioner "powershell" {
    use_pwsh = true
    script   = "eject-media.ps1"
  }

  provisioner "powershell" {
    use_pwsh = true
    script   = "optimize-2016.ps1"
  }

  provisioner "powershell" {
    inline = ["[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12", "Install-PackageProvider -Name NuGet -Force", "Install-Module PowerShellGet -Force -SkipPublisherCheck"]
  }
}
