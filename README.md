# This repo is an adaptation of [GOAD](https://github.com/Orange-Cyberdefense/GOAD) to work on vSphere environement

> this repo is based on [windows-vagrant](https://github.com/rgl/windows-vagrant)

# Deploy GOAD on vSphere with Packer, Govc and Vagrant

# My vSphere Network

## vSwitch

![](./img/Pasted%20image%2020221117043812.png)

## Firewall/Bastion

![](./img/Pasted%20image%2020221117044415.png)

> opnsense have 1 interface for each vSwitch on my lab with the ip x.x.x.254 (gateway)

- VM Network        => 1.25.52.254
- VM GOAD Network   => 192.168.56.254

## DHCP

> i have a dhcp configured on my VM Network
> this is required for packer and vagrant

# Ubuntu 20.4.5 ( for make a GOAD installer)

i will spawn an ubuntu 20.4.5 with 2 network insterface and install all requirement needed

![](./img/Pasted%20image%2020221117045442.png)


## installation

![](./img/Pasted%20image%2020221117060109.png)

## check mac address for management network

![](./img/Pasted%20image%2020221117060212.png)

![](./img/Pasted%20image%2020221117060259.png)

> ens160 will be my management interface

## Configure Static IP

### Management Network
![](./img/Pasted%20image%2020221117060640.png)

### GOAD Network
![](./img/Pasted%20image%2020221117060753.png)

![](./img/Pasted%20image%2020221117060825.png)

## Storage

![](./img/Pasted%20image%2020221117060926.png)

![](./img/Pasted%20image%2020221117061018.png)

> i will extend ubuntu-lv with the free space

![](./img/Pasted%20image%2020221117061146.png)

![](./img/Pasted%20image%2020221117061207.png)

![](./img/Pasted%20image%2020221117061226.png)

![](./img/Pasted%20image%2020221117061315.png)

## SSH

![](./img/Pasted%20image%2020221117061505.png)

>keep the rest by default and finish installation process

![](./img/Pasted%20image%2020221117064824.png)

>i will connect with vscode remote ssh to get a sweeter interface

## Install Git & Zsh


```
sudo apt update
sudo apt upgrade

sudo apt install git zsh
```

![](./img/Pasted%20image%2020221117070853.png)

```
curl -L https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh
```

![](./img/Pasted%20image%2020221117070938.png)

### customisation du shell

```
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${Zsh_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
git clone https://github.com/zsh-users/zsh-autosuggestions ${Zsh_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${Zsh_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
```

#### Edit .zshrc

```
ZSH_THEME="powerlevel10k/powerlevel10k"
```

![](./img/Pasted%20image%2020221117071244.png)

```
plugins=(git sudo zsh-syntax-highlighting zsh-autosuggestions)
```

![](./img/Pasted%20image%2020221117071334.png)

### make zsh as default shell

```
chsh -s $(which zsh)
```

![](./img/Pasted%20image%2020221117071435.png)

### swith to zsh

```
zsh
```

> and follow the wizard to configure the shell as you want

![](./img/Pasted%20image%2020221117071606.png)

> if you dont see the diamond you have to install the meslo font

```
https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf
```

### to get it working on vscode terminal

![](./img/Pasted%20image%2020221117071846.png)

> on the search bar type " terminal font"

> and add "MesloLGS NF"

![](./img/Pasted%20image%2020221117072026.png)

![](./img/Pasted%20image%2020221117072231.png)

> here we go

## Install  vagrant, packer, govc

### vagrant

```
wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
sudo apt update && sudo apt install vagrant
```

![](./img/Pasted%20image%2020221117073343.png)

### vagrant-vsphere & vagrant-windows-sysprep

```
sudo apt-get install build-essential patch ruby-dev zlib1g-dev liblzma-dev
vagrant plugin install vagrant-vsphere
vagrant plugin install vagrant-windows-sysprep
```

![](./img/Pasted%20image%2020221117074513.png)

### packer

```
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install packer
```

![](./img/Pasted%20image%2020221117074808.png)

### govc

```
sudo -s
curl -L -o - "https://github.com/vmware/govmomi/releases/latest/download/govc_$(uname -s)_$(uname -m).tar.gz" | tar -C /usr/local/bin -xvzf - govc
exit
```

![](./img/Pasted%20image%2020221117075231.png)

```
govc version
```

![](./img/Pasted%20image%2020221117075342.png)

## Clone repo

```
git clone https://gitlab.com/na.3/goad-vsphere.git
```

![](./img/Pasted%20image%2020221117075457.png)

# Build Packer template

## Download iso and upload them to the datastore

```
https://software-static.download.prss.microsoft.com/sg/download/17763.379.190312-0539.rs5_release_svc_refresh_SERVER_EVAL_x64FRE_fr-FR.iso
https://isofiles.bd581e55.workers.dev/Windows%20Server/Windows%20Server%202016/en_windows_server_2016_x64_dvd_9718492.iso
https://customerconnect.vmware.com/en/downloads/info/slug/datacenter_cloud_infrastructure/vmware_tools/12_x
```

![](./img/Pasted%20image%2020221117080342.png)

## make secret's files

```
cat >secrets.sh <<'EOF'
export GOVC_INSECURE='1'
export GOVC_HOST='vsphere.local'
export GOVC_URL="https://$GOVC_HOST/sdk"
export GOVC_USERNAME='administrator@vsphere.local'
export GOVC_PASSWORD='password'
export GOVC_DATACENTER='Datacenter'
export GOVC_CLUSTER='Cluster'
export GOVC_DATASTORE='Datastore'
export VSPHERE_ESXI_HOST='esxi.local'
export VSPHERE_TEMPLATE_FOLDER='test/templates'
export VSPHERE_NETWORK_ADAPTER_TYPE='vmxnet3'
# NB the VSPHERE_TEMPLATE_NAME last segment MUST match the
#    builders.vm_name property inside the packer tamplate.
export VSPHERE_TEMPLATE_NAME_SHORT="windows-2019-amd64-vsphere"
export VSPHERE_TEMPLATE_NAME="$VSPHERE_TEMPLATE_FOLDER/$VSPHERE_TEMPLATE_NAME_SHORT"
export VSPHERE_TEMPLATE_IPATH="//$GOVC_DATACENTER/vm/$VSPHERE_TEMPLATE_NAME"
export VSPHERE_VM_FOLDER='GOAD'
export VSPHERE_VM_NAME='windows-2019-vagrant-example'
export VSPHERE_MANAGE_VLAN='VM Network'
export VSPHERE_GOAD_VLAN='VM GOAD Network'
# set the credentials that the guest will use
# to connect to this host smb share.
# NB you should create a new local user named _vagrant_share
#    and use that one here instead of your user credentials.
# NB it would be nice for this user to have its credentials
#    automatically rotated, if you implement that feature,
#    let me known!
export VAGRANT_SMB_USERNAME='_vagrant_share'
export VAGRANT_SMB_PASSWORD=''
EOF
```

> i used a vm folder to store the template an anoter for the the vm's

![](./img/Pasted%20image%2020221117080612.png)

> folder GOAD for the VM's
> folder Modeles/Packer for the templates

### duplicate Secret file and edit it

![](./img/Pasted%20image%2020221117080923.png)

> use the example-secrets-2019.sh for edit your files
> tamper GOVC vars, Folders and Networks

>dont edit vm_name or template_name_short (need this syntax for the makefile)

![](./img/Pasted%20image%2020221117081249.png)

![](./img/Pasted%20image%2020221117081411.png)

## check govc connection

```
source secrets-2019.sh
# see https://github.com/vmware/govmomi/blob/master/govc/USAGE.md
govc version
govc about
govc datacenter.info # list datacenters
govc find # find all managed objects
```

![](./img/Pasted%20image%2020221117081530.png)

## Build Template with packer

> you have to edit .pkr.hcl file to fill the right path or your iso folder on the datastore

![](./img/Pasted%20image%2020221117082511.png)

![](./img/Pasted%20image%2020221117082551.png)


>you can build both at same time if you use 2 shell

```
source secrets-2019.sh
make build-windows-2019-vsphere

source secrets-2016.sh
make build-windows-2016-vsphere
```

> if you get this error:

```
Error: no plugin installed for github.com/rgl/windows-update 0.14.1

Did you run packer init for this project ?


make: *** [Makefile:39: windows-2019-amd64-vsphere.box] Error 1
rm tmp/windows-2019-vsphere/autounattend.xml
```

>do this:

```
chmod +w ~/.config/packer/plugins/github.com/rgl/windows-update/ -R
```

![](./img/Pasted%20image%2020221117084818.png)

>you can see the vm are spawned
>![](./img/Pasted%20image%2020221117084848.png)

> select the 2016 and open it on explorer

![](./img/Pasted%20image%2020221117084939.png)

> you have to press enter on " I don't have product key"

![](./img/Pasted%20image%2020221117085018.png)

> and the installation could begin

![](./img/Pasted%20image%2020221117085059.png)

> you dont neeed do this with 2019, i got an error when trying to do without the key on unatended file ( dont know why)

> as you can see the vagant dummy box are added automaticaly to vagrant store
> if you want rebuild the template you need to "vagrant box remove  " before 

![](./img/Pasted%20image%2020221117090455.png)

> the vm's have been converted as template

![](./img/Pasted%20image%2020221117090536.png)

# Provioning the lab with vagrant

```
cd vagrant
vagrant up --no-destroy-on-error --provider=vsphere
```

![](./img/Pasted%20image%2020221117090843.png)

> the vm are started

![](./img/Pasted%20image%2020221117091343.png)

> you can move them to a vapp to start the lab on 1 click and manage resource pools

![](./img/Pasted%20image%2020221117091748.png)

> after the sysprep step you need to click one more time on 2016 vm (DC03 & SRV03)

![](./img/Pasted%20image%2020221117091718.png)


![](./img/Pasted%20image%2020221117092013.png)


> after end of vagrant build you can verify if the vm's have the good static ip on the AD Network Interface

![](./img/Pasted%20image%2020221117092225.png)

# Ansible

## install Ansible and the prerequisite

```
cd ../ansible
sudo apt install python3.8-venv
wget https://bootstrap.pypa.io/get-pip.py
/usr/bin/python3.8 get-pip.py
/usr/bin/python3.8 -m pip install virtualenv
python3.8 -m virtualenv .venv
source .venv/bin/activate
```

![](./img/Pasted%20image%2020221117093707.png)

```
source .venv/bin/activate
```

![](./img/Pasted%20image%2020221117093828.png)

### once in the .venv

```
python3 -m pip install --upgrade pip
python3 -m pip install ansible-core==2.12.6
python3 -m pip install pywinrm

ansible-galaxy install -r requirements.yml
```

# Run Ansible

```
ansible-playbook main.yml
```

![](./img/Pasted%20image%2020221117094330.png)

> now wait like 2 hours and let the magic working...

> if you get some error like that , re run the playbook

![](./img/Pasted%20image%2020221117094558.png)

> i got all working on the second pass

![](./img/Pasted%20image%2020221117094647.png)


