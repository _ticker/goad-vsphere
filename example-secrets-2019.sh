export GOVC_INSECURE='1'
export GOVC_HOST='1.25.52.10'
export GOVC_URL="https://$GOVC_HOST/sdk"
export GOVC_USERNAME='administrator@vsphere.local'
export GOVC_PASSWORD='xxxxxxxxx'
export GOVC_DATACENTER='Kore'
export GOVC_CLUSTER='Home'
export GOVC_DATASTORE='NVME-1'
export VSPHERE_ESXI_HOST='1.25.52.1'
export VSPHERE_TEMPLATE_FOLDER='Modeles/Packer'
export VSPHERE_NETWORK_ADAPTER_TYPE='vmxnet3'
# NB the VSPHERE_TEMPLATE_NAME last segment MUST match the
#    builders.vm_name property inside the packer tamplate.
export VSPHERE_TEMPLATE_NAME_SHORT="windows-2019-amd64-vsphere"
export VSPHERE_TEMPLATE_NAME="$VSPHERE_TEMPLATE_FOLDER/$VSPHERE_TEMPLATE_NAME_SHORT"
export VSPHERE_TEMPLATE_IPATH="//$GOVC_DATACENTER/vm/$VSPHERE_TEMPLATE_NAME"
export VSPHERE_VM_FOLDER='GOAD'
export VSPHERE_VM_NAME='windows-2019-vagrant-example'
export VSPHERE_MANAGE_VLAN='VM Network'
export VSPHERE_GOAD_VLAN='VM GOAD Network'
# set the credentials that the guest will use
# to connect to this host smb share.
# NB you should create a new local user named _vagrant_share
#    and use that one here instead of your user credentials.
# NB it would be nice for this user to have its credentials
#    automatically rotated, if you implement that feature,
#    let me known!
export VAGRANT_SMB_USERNAME='_vagrant_share'
export VAGRANT_SMB_PASSWORD=''
